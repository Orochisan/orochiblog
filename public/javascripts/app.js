(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';

    if (has(cache, path)) return cache[path].exports;
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex].exports;
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  var define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  var list = function() {
    var result = [];
    for (var item in modules) {
      if (has(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  globals.require = require;
  globals.require.define = define;
  globals.require.register = define;
  globals.require.list = list;
  globals.require.brunch = true;
})();
require.register("app", function(exports, require, module) {

/**
  The application object

  @class App
  @extends Ember.Application
 */
var App;

module.exports = App = Ember.Application.create({

  /**
    Name of the application
  
    @property name
    @type String
   */
  name: 'Brunch with Ember and Bootstrap'
});
});

;require.register("components", function(exports, require, module) {
require('views/components/BsIcon');

require('templates/components/bs-icon');
});

;require.register("config", function(exports, require, module) {
var App;

App = require('app');

Ember.ENV.HELPER_PARAM_LOOKUPS = true;


/**
  Some configuration

  @namespace App
  @property CONFIG
  @type Object
  @final
 */

module.exports = App.CONFIG = {

  /**
    Some compilation information (look in /config.coffee for more
    information about the keyword plugin configuration)
  
    @property compilation
    @type Object
    @final
   */
  compilation: {

    /**
      Will be replaced with the name of the git current branch
      @property gitBranch
      @type String
      @final
     */
    gitBranch: 'n/a',

    /**
      Will be replaced with the hash of the last git commit
      @property gitCommitHash
      @type String
      @final
     */
    gitCommitHash: 'n/a',

    /**
      Will be replaced with the small version of the hash of the last git commit
      @property gitShortCommitHash
      @type String
      @final
     */
    gitShortCommitHash: 'n/a',

    /**
      Will be replaced with the compilation date and time
      @property date
      @type Date
      @final
     */
    date: new Date('Wed, 21 May 2014 07:53:48 GMT')
  }
};
});

;require.register("controllers", function(exports, require, module) {

});

;require.register("helpers", function(exports, require, module) {

});

;require.register("initialize", function(exports, require, module) {
require('app');

require('config');

require('router');

require('routes');

require('store');

require('models');

require('components');

require('views');

require('controllers');

require('helpers');

require('templates');
});

;require.register("models", function(exports, require, module) {

});

;require.register("router", function(exports, require, module) {
var App;

App = require('app');

App.Router.map(function() {});
});

;require.register("routes", function(exports, require, module) {
require('routes/Index');
});

;require.register("routes/Index", function(exports, require, module) {
var App;

App = require('app');


/**
  Route IndexRoute

  @class IndexRoute
  @namespace App
  @extends Ember.Route
 */

module.exports = App.IndexRoute = Ember.Route.extend({

  /**
    Our model, just some info message as of example and all the font awesome icons
  
    @inheritDoc
   */
  model: function(params) {
    return {
      infoMessage: "Compiled on branch " + App.CONFIG.compilation.gitBranch + "\nat " + (App.CONFIG.compilation.date.toTimeString()) + "\non " + (App.CONFIG.compilation.date.toDateString()),
      iconNames: Ember.A(App.BsIconComponent.NAMES)
    };
  }
});
});

;require.register("store", function(exports, require, module) {
var App;

App = require('app');


/*
 * Remove this return to enable a store with a REST adapter on your app
 * if you're migrating, https://github.com/emberjs/data/blob/master/TRANSITION.md is a good help
 */

return;


/**
  The application adapter

  @class ApplicationAdapter
  @namespace App
  @extends DS.RESTAdapter
 */

App.ApplicationAdapter = DS.RESTAdapter.extend({

  /**
    Put the host of your API here
  
    @inheritDoc
   */
  host: 'http://www.google.com',

  /**
    Put the path/namespace of your API here
  
    @inheritDoc
   */
  namespace: 'api/v1'
});


/**
  If you need to customize the serializer for any reason, here is where it should go:

  @class ApplicationSerializer
  @namespace App
  @extends DS.RESTSerializer
 */

App.ApplicationSerializer = DS.RESTSerializer.extend;
});

;require.register("templates", function(exports, require, module) {
require('templates/application');

require('templates/index');
});

;require.register("templates/application", function(exports, require, module) {
module.exports = Ember.TEMPLATES['application'] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', hashTypes, hashContexts, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class=\"container\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "outlet", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</div>");
  return buffer;
  
});
});

;require.register("templates/components/bs-icon", function(exports, require, module) {
module.exports = Ember.TEMPLATES['components/bs-icon'] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '';


  data.buffer.push("\n");
  return buffer;
  
});
});

;require.register("templates/index", function(exports, require, module) {
module.exports = Ember.TEMPLATES['index'] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, stack2, hashContexts, hashTypes, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1, hashContexts, hashTypes, options;
  data.buffer.push("\n    <div class=\"col-xs-6 col-sm-4 col-md-3 col-lg-2\">\n      <div class=\"row\">\n        <div class=\"col-xs-3 col-sm-2 text-center\">");
  hashContexts = {'name': depth0,'type': depth0,'class': depth0};
  hashTypes = {'name': "ID",'type': "STRING",'class': "STRING"};
  options = {hash:{
    'name': ("name"),
    'type': ("success"),
    'class': ("icon-large")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bs-icon'] || (depth0 && depth0['bs-icon'])),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bs-icon", options))));
  data.buffer.push("</div>\n        <div class=\"col-xs-9 col-sm-10\">");
  hashTypes = {};
  hashContexts = {};
  data.buffer.push(escapeExpression(helpers._triageMustache.call(depth0, "name", {hash:{},contexts:[depth0],types:["ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("</div>\n      </div>\n    </div>\n  ");
  return buffer;
  }

  data.buffer.push("<h2>Welcome to brunch with Ember and Bootstrap</h2>\n\n<div class=\"well\">\n  <h4>\n    With keyword-brunch for compile time keyword replacement\n  </h4>\n  ");
  hashContexts = {'type': depth0,'messageBinding': depth0};
  hashTypes = {'type': "STRING",'messageBinding': "STRING"};
  data.buffer.push(escapeExpression(helpers.view.call(depth0, "Bootstrap.AlertMessage", {hash:{
    'type': ("info"),
    'messageBinding': ("infoMessage")
  },contexts:[depth0],types:["STRING"],hashContexts:hashContexts,hashTypes:hashTypes,data:data})));
  data.buffer.push("\n</div>\n\n<div class=\"well\">\n  <h4>\n    With Font Awesome 3\n  </h4>\n  <pre>{{bs-icon name=\"thumbs-up\" type=\"danger\"}} ");
  hashContexts = {'name': depth0,'type': depth0};
  hashTypes = {'name': "STRING",'type': "STRING"};
  options = {hash:{
    'name': ("thumbs-up"),
    'type': ("danger")
  },contexts:[],types:[],hashContexts:hashContexts,hashTypes:hashTypes,data:data};
  data.buffer.push(escapeExpression(((stack1 = helpers['bs-icon'] || (depth0 && depth0['bs-icon'])),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "bs-icon", options))));
  data.buffer.push("</pre>\n  <div class=\"row\">\n  ");
  hashTypes = {};
  hashContexts = {};
  stack2 = helpers.each.call(depth0, "name", "in", "iconNames", {hash:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],hashContexts:hashContexts,hashTypes:hashTypes,data:data});
  if(stack2 || stack2 === 0) { data.buffer.push(stack2); }
  data.buffer.push("\n  </div>\n</div>\n");
  return buffer;
  
});
});

;require.register("views", function(exports, require, module) {
require('views/Application');
});

;require.register("views/Application", function(exports, require, module) {
var App;

App = require('app');


/**
  `ApplicationView` is the main view, understand the bottom layer,
  container of all the applicaiton

  @class ApplicationView
  @namespace App
  @extends Ember.View
 */

module.exports = App.ApplicationView = Ember.View.extend({

  /**
    The current path is used to set an attribute in the base application view: `data-path`
    This can be useful in some cases
  
    @property attributeBindings
    @type Array
   */
  attributeBindings: ['currentPath:data-path'],

  /**
    Our composed `routeClasses` property is binded to css classes here
  
    @see {App.ApplicationView#routeClasses}
    @property classNameBindings
    @type Array
   */
  classNameBindings: ['routeClasses'],

  /**
    Get the current path from our controller which is `ApplicationController`
  
    @property currentPath
    @type String
   */
  currentPath: '',
  currentPathBinding: Em.Binding.oneWay('controller.currentPath'),

  /**
    Returns the classes depending on the current path in the router. For example if we're in path
    `users.user.profile.show` the classes added to the application view would be:
    `resource-users resource-user resource-profile route-show`.
    Note that the last one which is always a route is prepended with `route-` instead of `resource-`.
  
    This allow you to customize any part of your application depending on the current resource,
    sub-resource and/or even route.
  
    @property routeClasses
    @type String
   */
  routeClasses: (function() {
    var cl, classes, currentPath, resources, route, _ref;
    currentPath = this.get('currentPath');
    resources = (_ref = currentPath != null ? currentPath.split(/\./g) : void 0) != null ? _ref : [];
    route = resources.pop();
    classes = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = resources.length; _i < _len; _i++) {
        cl = resources[_i];
        _results.push("resource-" + cl);
      }
      return _results;
    })();
    if (route) {
      classes.push("route-" + route);
    }
    return classes.join(' ');
  }).property('currentPath').readOnly()
});
});

;require.register("views/components/BsIcon", function(exports, require, module) {
var App;

App = require('app');


/**
  Component BsIconComponent to insert bound font awesome icons
  Example:
  ```handlebars
  {{bs-icon name="user" type="info"}}

  @class BsIconComponent
  @namespace App
  @extends Ember.Component
 */

module.exports = App.BsIconComponent = Ember.Component.extend(Bootstrap.TypeSupport, {

  /**
    Our tag name is an <i> element
  
    @inheritDoc
   */
  tagName: 'i',

  /**
    We bind the icon name and some other options depending on the css class
  
    @inheritDoc
   */
  classNameBindings: ['iconClass', 'border:icon-border', 'flipHorizontal:icon-flip-horizontal', 'flipVertical:icon-flip-vertical', 'spin:icon-spin'],

  /**
    The base class name for {Bootstrap.TypeSupport}
  
    @property baseClassName
    @type String
   */
  baseClassName: 'text',

  /**
    Set this to true and the icon will be rotating
  
    @property spin
    @type Boolean
   */
  spin: false,

  /**
    Should the icon have a border?
  
    @property border
    @type Boolean
   */
  border: false,

  /**
    Set this to true and the icon will be filpped horizontally
  
    @property flipHorizontal
    @type Boolean
   */
  flipHorizontal: false,

  /**
    Set this to true and the icon will be flipped vertically
  
    @property flipVertical
    @type Boolean
   */
  flipVertical: false,

  /**
    Name of the icon based on the name given in context
  
    @property iconClass
    @type String
   */
  iconClass: (function() {
    return "icon-" + (this.get('name'));
  }).property('name')
});

App.BsIconComponent.reopenClass({

  /**
    All the possible icon names
  
    @property NAMES
    @type Array
    @static
    @final
   */
  NAMES: ['glass', 'music', 'search', 'envelope', 'heart', 'star', 'star-empty', 'user', 'film', 'th-large', 'th', 'th-list', 'ok', 'remove', 'zoom-in', 'zoom-out', 'off', 'signal', 'cog', 'trash', 'home', 'file', 'time', 'road', 'download-alt', 'download', 'upload', 'inbox', 'play-circle', 'repeat', 'rotate-right', 'refresh', 'list-alt', 'lock', 'flag', 'headphones', 'volume-off', 'volume-down', 'volume-up', 'qrcode', 'barcode', 'tag', 'tags', 'book', 'bookmark', 'print', 'camera', 'font', 'bold', 'italic', 'text-height', 'text-width', 'align-left', 'align-center', 'align-right', 'align-justify', 'list', 'indent-left', 'indent-right', 'facetime-video', 'picture', 'pencil', 'map-marker', 'adjust', 'tint', 'edit', 'share', 'check', 'move', 'step-backward', 'fast-backward', 'backward', 'play', 'pause', 'stop', 'forward', 'fast-forward', 'step-forward', 'eject', 'chevron-left', 'chevron-right', 'plus-sign', 'minus-sign', 'remove-sign', 'ok-sign', 'question-sign', 'info-sign', 'screenshot', 'remove-circle', 'ok-circle', 'ban-circle', 'arrow-left', 'arrow-right', 'arrow-up', 'arrow-down', 'share-alt', 'mail-forward', 'resize-full', 'resize-small', 'plus', 'minus', 'asterisk', 'exclamation-sign', 'gift', 'leaf', 'fire', 'eye-open', 'eye-close', 'warning-sign', 'plane', 'calendar', 'random', 'comment', 'magnet', 'chevron-up', 'chevron-down', 'retweet', 'shopping-cart', 'folder-close', 'folder-open', 'resize-vertical', 'resize-horizontal', 'bar-chart', 'twitter-sign', 'facebook-sign', 'camera-retro', 'key', 'cogs', 'comments', 'thumbs-up', 'thumbs-down', 'star-half', 'heart-empty', 'signout', 'linkedin-sign', 'pushpin', 'external-link', 'signin', 'trophy', 'github-sign', 'upload-alt', 'lemon', 'phone', 'check-empty', 'bookmark-empty', 'phone-sign', 'twitter', 'facebook', 'github', 'unlock', 'credit-card', 'rss', 'hdd', 'bullhorn', 'bell', 'certificate', 'hand-right', 'hand-left', 'hand-up', 'hand-down', 'circle-arrow-left', 'circle-arrow-right', 'circle-arrow-up', 'circle-arrow-down', 'globe', 'wrench', 'tasks', 'filter', 'briefcase', 'fullscreen', 'group', 'link', 'cloud', 'beaker', 'cut', 'copy', 'paper-clip', 'save', 'sign-blank', 'reorder', 'list-ul', 'list-ol', 'strikethrough', 'underline', 'table', 'magic', 'truck', 'pinterest', 'pinterest-sign', 'google-plus-sign', 'google-plus', 'money', 'caret-down', 'caret-up', 'caret-left', 'caret-right', 'columns', 'sort', 'sort-down', 'sort-up', 'envelope-alt', 'linkedin', 'undo', 'rotate-left', 'legal', 'dashboard', 'comment-alt', 'comments-alt', 'bolt', 'sitemap', 'umbrella', 'paste', 'lightbulb', 'exchange', 'cloud-download', 'cloud-upload', 'user-md', 'stethoscope', 'suitcase', 'bell-alt', 'coffee', 'food', 'file-alt', 'building', 'hospital', 'ambulance', 'medkit', 'fighter-jet', 'beer', 'h-sign', 'plus-sign-alt', 'double-angle-left', 'double-angle-right', 'double-angle-up', 'double-angle-down', 'angle-left', 'angle-right', 'angle-up', 'angle-down', 'desktop', 'laptop', 'tablet', 'mobile-phone', 'circle-blank', 'quote-left', 'quote-right', 'spinner', 'circle', 'reply', 'mail-reply', 'folder-close-alt', 'folder-open-alt', 'expand-alt', 'collapse-alt', 'smile', 'frown', 'meh', 'gamepad', 'keyboard', 'flag-alt', 'flag-checkered', 'terminal', 'code', 'reply-all', 'mail-reply-all', 'star-half-empty', 'location-arrow', 'crop', 'code-fork', 'unlink', 'question', 'info', 'exclamation', 'superscript', 'subscript', 'eraser', 'puzzle-piece', 'microphone', 'microphone-off', 'shield', 'calendar-empty', 'fire-extinguisher', 'rocket', 'maxcdn', 'chevron-sign-left', 'chevron-sign-right', 'chevron-sign-up', 'chevron-sign-down', 'html5', 'css3', 'anchor', 'unlock-alt', 'bullseye', 'ellipsis-horizontal', 'ellipsis-vertical', 'rss-sign', 'play-sign', 'ticket', 'minus-sign-alt', 'check-minus', 'level-up', 'level-down', 'check-sign', 'edit-sign', 'external-link-sign', 'share-sign'].sort()
});
});

;
//# sourceMappingURL=app.js.map